var express = require('express');
var router = express.Router();

let users = [ {
      "avatar" : "https://www.gravatar.com/avatar/6uM9nyXmLvg3e0b8Q4LQwsCfEQuXnCC9.jpg?d=retro",
      "email" : "cazaustre@gmail.com",
      "uid" : "JR5qy1bpRLaItBj3WnWXPzlYucr1"
    },
    {
      "avatar" : "https://www.gravatar.com/avatar/aAvCItEqBcUZl05MtM5qIpo3QgArye9a.jpg?d=retro",
      "email" : "paola@chefly.co",
      "uid" : "cJKhJOhrHoTnKJfQkR797LFQOER2"
    } ];
/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

/* Get user by email */
router.get('/:email', function(req, res) {
  res.send(users.filter( user => user.email == req.params.email ));
});

/* Create a new user */
router.post('/create', function(req, res) {
  const avatar = req.body.avatar;
  const email = req.body.email;
  const uid = req.body.uid;

  const new_user = {"avatar": avatar, "email": email, "uid":uid};

  users = users.concat(new_user);

  res.send(new_user);
});

module.exports = router;
