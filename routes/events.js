var express = require('express');
var router = express.Router();

const events = [ {
    "date" : 1500501600000,
    "description" : "Iniciamos la andadura de charlas de la comunidad VueJS Madrid con la mayor ilusión posible y con ganas de comernos el mundo del desarrollo web Front-End con el framework que aúna las mejores ideas de Angular y React con un API sencillo, elegante, moderno y para toda la familia",
    "groupId" : 3,
    "groupImage" : "https://firebasestorage.googleapis.com/v0/b/meetup-796d3.appspot.com/o/groups%2Fglobal_458596778.png?alt=media&token=01351360-ea8a-4168-ac3f-a0de2942f54a",
    "groupName" : "VueJS Madrid",
    "id" : 1,
    "location" : "Telefonica Flag Store",
    "locationAddress" : "Calle Gran Via 28, Madrid",
    "title" : "Introducción a Vue.js"
  }, {
    "date" : 1600501600000,
    "description" : "Si estás emocionado por el estreno de la siguiente temporada de Juego de Tronos, ¡Nosotros también! por eso te esperamos el próximo 19 de Julio en Campus Madrid para celebrar su regreso y aprender TensorFlow al mismo tiempo.",
    "groupId" : 2,
    "groupImage" : "https://firebasestorage.googleapis.com/v0/b/meetup-796d3.appspot.com/o/groups%2Fglobal_446339090.png?alt=media&token=8d3c838b-4c21-4742-adcf-40b09813be7a",
    "groupName" : "GDG Madrid",
    "id" : 2,
    "location" : "Campus Madrid",
    "locationAddress" : "Calle Moreno Nieto 2, Madrid",
    "title" : "Codelab: Juego de Tronos y Tensor Flow"
  }, {
    "date" : 1700501600000,
    "description" : "En el evento de hoy veremos como podemos desarrollar desde cero y en poco tiempo una aplicación similar a HotDogOrNot de la serie de HBO Silicon Valley, utilizando React, Firebase, Firebase Cloud Functions y el API de Machine Learning Google Cloud Vision",
    "groupId" : 5,
    "groupImage" : "https://firebasestorage.googleapis.com/v0/b/meetup-796d3.appspot.com/o/groups%2Fglobal_450181984.png?alt=media&token=ae806d6e-2d74-4474-8c6b-8a0258faf6e7",
    "groupName" : "Machine Learning Spain",
    "id" : 3,
    "location" : "Campus Madrid",
    "locationAddress" : "Calle Moreno Nieto 2, Madrid",
    "title" : "Cómo hacer Hotdog or not App"
  }, {
    "date" : 1800501600000,
    "description" : "GDG Madrid invites you to attend our next event, this time, the main theme is Firebase. Google is sending Carlos Azaustre and Paola Garcia from Chefly in Madrid, to Aalborg to perform two sessions of Firebase codelab. They will be starting from scratch so a basic programming level is enough.",
    "groupId" : 2,
    "groupImage" : "https://firebasestorage.googleapis.com/v0/b/meetup-796d3.appspot.com/o/groups%2Fglobal_446339090.png?alt=media&token=8d3c838b-4c21-4742-adcf-40b09813be7a",
    "groupName" : "GDG Madrid",
    "id" : 4,
    "location" : "Centro de Innovación BBVA",
    "locationAddress" : "Plaza de Santa Bárbara 2, Madrid",
    "title" : "Tu primera Cloud Function"
  }, {
    "date" : 1900501600000,
    "description" : "En el evento de hoy veremos una sencilla introducción a GraphQL un nuevo sistema de queries que puede sustituir a las API REST en un futuro",
    "groupId" : 4,
    "groupImage" : "https://firebasestorage.googleapis.com/v0/b/meetup-796d3.appspot.com/o/groups%2Fglobal_457271152.png?alt=media&token=e279ade7-cf60-4ce0-8d74-048295dafe7c",
    "groupName" : "ReactJS Madrid",
    "id" : 5,
    "location" : "Billin HQ",
    "locationAddress" : "Calle Talavera 4, Loft 2, Madrid",
    "title" : "Introducción a GraphQL"
  }, {
    "date" : 2000501600000,
    "description" : "En nuestro último encuentro antes del verano, rescatamos una propuesta que teníamos desde Septiembre del año pasado del gran Israel Gutiérrez. Y si no nos vemos por Kunlabori, os deseamos feliz verano y próspero curso nuevo!",
    "groupId" : 1,
    "groupImage" : "https://firebasestorage.googleapis.com/v0/b/meetup-796d3.appspot.com/o/groups%2Fglobal_139815902.png?alt=media&token=66c7a268-f057-46bc-8fea-1cd73c05cb64",
    "groupName" : "MadridJS",
    "id" : 6,
    "location" : "Kunlabori Coworking",
    "locationAddress" : "Calle Eduardo Vicente 7, Madrid",
    "title" : "Acercamiento con sigilo a la programación funcional"
  }, {
    "date" : 2100501600000,
    "description" : "Iniciamos la andadura de charlas de la comunidad VueJS Madrid con la mayor ilusión posible y con ganas de comernos el mundo del desarrollo web Front-End con el framework que aúna las mejores ideas de Angular y React con un API sencillo, elegante, moderno y para toda la familia",
    "groupId" : 3,
    "groupImage" : "https://firebasestorage.googleapis.com/v0/b/meetup-796d3.appspot.com/o/groups%2Fglobal_458596778.png?alt=media&token=01351360-ea8a-4168-ac3f-a0de2942f54a",
    "groupName" : "VueJS Madrid",
    "id" : 7,
    "location" : "Telefonica Flag Store",
    "locationAddress" : "Calle Gran Via 27, Madrid",
    "title" : "Server Rendering con Vue.js"
  } ];

/* GET events listing. */
router.get('/', function(req, res, next) {
  res.send(events);
});



/* Get events by id */
router.get('/:id', function(req, res) {
  res.send(events.filter( event => event.id == req.params.id ));
});

module.exports = router;
