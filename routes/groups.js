var express = require('express');
var router = express.Router();

let groups = [ {
    "about" : "Nos gusta la programación, nos apasiona JavaScript y vivimos en Madrid. Si coincides en 2 de estas 3 cosas pásate por el grupo o mejor todavía ¡Envía una propuesta!",
    "id" : 1,
    "image" : "https://firebasestorage.googleapis.com/v0/b/meetup-796d3.appspot.com/o/groups%2Fglobal_139815902.png?alt=media&token=66c7a268-f057-46bc-8fea-1cd73c05cb64",
    "name" : "MadridJS"
  }, {
    "about" : "El Google Developer Group de Madrid es un grupo sin ánimo de lucro, formado por desarrolladores de Madrid que tienen el objetivo de aprender e intercambiar conocimientos y experiencias acerca de las tecnologías de Google. La participación es abierta y gratuita.",
    "id" : 2,
    "image" : "https://firebasestorage.googleapis.com/v0/b/meetup-796d3.appspot.com/o/groups%2Fglobal_446339090.png?alt=media&token=8d3c838b-4c21-4742-adcf-40b09813be7a",
    "name" : "GDG Madrid"
  }, {
    "about" : "VueJS es el framework JavaScript con mayor proyección en la actualidad. SI disfrutas construyendo Interfaces de Usuario con la última tecnología y quieres saber más sobre este apasionante framework. ¡Éste es tu grupo!",
    "id" : 3,
    "image" : "https://firebasestorage.googleapis.com/v0/b/meetup-796d3.appspot.com/o/groups%2Fglobal_458596778.png?alt=media&token=01351360-ea8a-4168-ac3f-a0de2942f54a",
    "name" : "VueJS Madrid"
  }, {
    "about" : "¿Te gusta ReactJS y todo lo que tiene que ver con ésta tecnología? Este es tu grupo! Organizamos charlas temáticas relacionadas con ReactJS y todo su ecosistema.",
    "id" : 4,
    "image" : "https://firebasestorage.googleapis.com/v0/b/meetup-796d3.appspot.com/o/groups%2Fglobal_457271152.png?alt=media&token=e279ade7-cf60-4ce0-8d74-048295dafe7c",
    "name" : "ReactJS Madrid"
  }, {
    "about" : "Grupo de Machine Learning en Madrid",
    "id" : 5,
    "image" : "https://firebasestorage.googleapis.com/v0/b/meetup-796d3.appspot.com/o/groups%2Fglobal_450181984.png?alt=media&token=ae806d6e-2d74-4474-8c6b-8a0258faf6e7",
    "name" : "Machine Learning Spain"
  } ];

  /* GET groups listing. */
  router.get('/', function(req, res, next) {
    res.send(groups);
  });



  /* Get groups by id */
  router.get('/:id', function(req, res) {
    res.send(groups.filter( event => groups.id == req.params.id ));
  });

  module.exports = router;
